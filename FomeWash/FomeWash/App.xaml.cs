﻿using Prism;
using Prism.Ioc;
using FomeWash.ViewModels;
using FomeWash.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FomeWash
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            
            await NavigationService.NavigateAsync("Inicio");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<Login>();
            containerRegistry.RegisterForNavigation<Dashboard>();
            containerRegistry.RegisterForNavigation<Registro>();
            containerRegistry.RegisterForNavigation<Mapa>();
            containerRegistry.RegisterForNavigation<DetalleServicio>();
            containerRegistry.RegisterForNavigation<MisServicios>();
            containerRegistry.RegisterForNavigation<Productos>();
            containerRegistry.RegisterForNavigation<Promociones>();
            containerRegistry.RegisterForNavigation<Ajustes>();
            containerRegistry.RegisterForNavigation<Inicio>();
            containerRegistry.RegisterForNavigation<SideMenu, SideMenuViewModel>();
            containerRegistry.RegisterForNavigation<Welcome, WelcomeViewModel>();;
            containerRegistry.RegisterForNavigation<Historial, HistorialViewModel>();
            containerRegistry.RegisterForNavigation<ServiciosPendientes, ServiciosPendientesViewModel>();
        }
    }
}
