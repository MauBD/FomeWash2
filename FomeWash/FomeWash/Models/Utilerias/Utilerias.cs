﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FomeWash.Models.Utilerias
{
    class Utilerias
    {
        public static bool validarcorreo(string mail)
        {
            Regex val = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (!String.IsNullOrWhiteSpace(mail))
            {
                if (val.IsMatch(mail))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public static bool validarString(string str)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


    }


    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string IdUsuarioKey = "IdUsuario_key";
        private static readonly string IdUsuarioDefault = string.Empty;

        private const string Nombre = "nombre_key";
        private static readonly string NombreDefault = string.Empty;

        private const string Foto = "foto_key";
        private static readonly string FotoDefault = "usr.png";

        private const string Correo = "correo_key";
        private static readonly string CorreoDefault = string.Empty;

        private const string Password = "password_key";
        private static readonly string PasswordDefault = string.Empty;


        #endregion


        public static string IdUsuarioSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(IdUsuarioKey, IdUsuarioDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IdUsuarioKey, value);
            }
        }

        public static string NombreSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Nombre, NombreDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Nombre, value);
            }
        }

        public static string FotoSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Foto, FotoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Foto, value);
            }
        }

        public static string CorreoSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Correo, CorreoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Correo, value);
            }
        }

        public static string PasswordSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Password, PasswordDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Password, value);
            }
        }


    }

}
