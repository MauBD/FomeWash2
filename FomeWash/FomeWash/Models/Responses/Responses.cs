﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FomeWash.Models.Responses
{
    class ResponseLogin
    {
        public bool exito { get; set; }
        public string usuario { get; set; }
        public string nombre { get; set; }
    }
    public class Escuela
    {
        public string Nombre { get; set; }
    }

    public class RegistroModel
    {
        public bool success { get; set; }
        public string msg { get; set; }
    }

    public class HorariosModel
    {
        public string idHorario { get; set; }
        public string hora { get; set; }
    }

    public class ResponseAgendar
    {
        public bool success { get; set; }
    }

    public class ResponseServicios
    {
        public string IdProductoServicio { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        string costo;
        public string Costo { get { return costo; } set { costo = "$" + value; } }

    }

    public class CitasResponse
    {
        public string IdServicio { get; set; }
        public string IdCliente { get; set; }
        public string Fecha { get; set; }
        public string Direccion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Subtotal { get; set; }
        public string Descuento { get; set; }
        string total;
        public string Total { get { return total; } set { total = "$" + value; } }
        public string Cupon { get; set; }
        public object IdOperador { get; set; }
        public object HoraEtimada { get; set; }
        public string TipoPago { get; set; }
        public string IdProductoServicio { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Costo { get; set; }
    }

    public class ResponsePromos
    {
        public string IdPromocion { get; set; }
        public string Cupon { get; set; }
        public string Descuento { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        

    }

    public class ResponseCupon
    {
        public bool success { get; set; }
        public string cupon { get; set; }
        public string descuento { get; set; }
        public string IdCupon { get; set; }
    }

    //clases para places autocomplete
    public class MatchedSubstring
    {
        public int length { get; set; }
        public int offset { get; set; }
    }

    public class MainTextMatchedSubstring
    {
        public int length { get; set; }
        public int offset { get; set; }
    }

    public class StructuredFormatting
    {
        public string main_text { get; set; }
        public List<MainTextMatchedSubstring> main_text_matched_substrings { get; set; }
        public string secondary_text { get; set; }
    }

    public class Term
    {
        public int offset { get; set; }
        public string value { get; set; }
    }

    public class Prediction
    {
        public string description { get; set; }
        public string id { get; set; }
        public List<MatchedSubstring> matched_substrings { get; set; }
        public string place_id { get; set; }
        public string reference { get; set; }
        public StructuredFormatting structured_formatting { get; set; }
        public List<Term> terms { get; set; }
        public List<string> types { get; set; }
    }

    public class ResponseAutocompletar
    {
        public List<Prediction> predictions { get; set; }
        public string status { get; set; }
    }

    //Clases para eocodin 
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    /*public class Geometry
    {
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    } 

     public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }*/

    public class ResponseGeocoding
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }



    //clases para eocodin latln a direccion
    /*public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }*/

    /*public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }*/

    /*public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }*/

    /*public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }*/

    public class Northeast2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast2 northeast { get; set; }
        public Southwest2 southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
        public Bounds bounds { get; set; }
    }

    public class Result
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }

    public class ResponseLatLonToAdreess
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
}
