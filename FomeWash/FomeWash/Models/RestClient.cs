﻿using Acr.UserDialogs;
using FomeWash.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TK.CustomMap;

namespace FomeWash.Models
{
    class RestClient
    {
        const string baseUrl = "http://www.fomewash.com.mx/panel/Appws/"; 
        //const string baseUrl = "http://www.fomewash.com.mx/fomewash/Appws/";


       

        public static async Task<List<ResponsePromos>> getPromociones()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(baseUrl + "promociones");

            List<ResponsePromos> data = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<ResponsePromos>>(json);
            }

            return data;
        }

        public static async Task<List<ResponseServicios>> getServicios()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(baseUrl + "servicios");

            List<ResponseServicios> data = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<List<ResponseServicios>>(json);
            }

            return data;
        }

        public static async Task<ResponseLogin> login(string usr, string pwd)
        {
            var formContent = new FormUrlEncodedContent(new[]
           {
                   new KeyValuePair<string,string>("usr", usr),
                   new KeyValuePair<string,string>("pwd", pwd)
           });
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "login", formContent);

            ResponseLogin data = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<ResponseLogin>(json);
            }

            return data;
        }

        public static async Task<RegistroModel> newRegistro(string nombre, string correo, string telefono, string pwd)
        {
            var formContent = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("nombre", nombre),
                new KeyValuePair<string, string>("mail", correo),
                new KeyValuePair<string, string>("tel", telefono),
                new KeyValuePair<string, string>("pwd", pwd)
            });

            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "registro", formContent);

            RegistroModel a = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                a = JsonConvert.DeserializeObject<RegistroModel>(json);
            }
            return a;
        }

        

        public static async Task<ResponseAgendar> agendarCita(string idUsr, string fecha, string direccion, double latitud, double longitud, string subtotal, string cupon,double descuento, string total, int tipopago,string IdProductoServicio)
        {
            var formContent = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("usr", idUsr),
                new KeyValuePair<string, string>("dia", fecha),
                new KeyValuePair<string, string>("dir", direccion),
                new KeyValuePair<string, string>("lat", latitud.ToString()),
                new KeyValuePair<string, string>("lon", longitud.ToString()),
                new KeyValuePair<string, string>("subt", subtotal.ToString()),
                new KeyValuePair<string, string>("desc", descuento.ToString()),
                new KeyValuePair<string, string>("total", total.ToString()),
                new KeyValuePair<string, string>("tipopago", tipopago.ToString()),
                new KeyValuePair<string, string>("cupon", cupon),
                new KeyValuePair<string, string>("proser", IdProductoServicio)
            });
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "agendar", formContent);
            
            ResponseAgendar aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<ResponseAgendar>(json);
            }

            return aux;
        }

        public static async Task<RegistroModel> cambiarPwd(int idUsr, string oldpwd, string newpwd)
        {
            var formContent = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("usr", idUsr.ToString()),
                new KeyValuePair<string, string>("oldpwd", oldpwd),
                new KeyValuePair<string, string>("newpwd", newpwd)
            });
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "cambiarPwd", formContent);

            RegistroModel aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<RegistroModel>(json);
            }

            return aux;
        }

        public static async Task<RegistroModel> recuperarPwd(string mail)
        {
            var formContent = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("mail", mail)
            });
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "recuperarPwd", formContent);

            RegistroModel aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<RegistroModel>(json);
            }

            return aux;
        }

        public static async Task<List<CitasResponse>> getCitasById(string usr)
        {
            var formContent = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("usr", usr) });

            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "proximasCitasById", formContent);

            List<CitasResponse> aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<List<CitasResponse>>(json);
            }
            return aux;
        }

        public static async Task<List<CitasResponse>> getCitasPasadasById(string usr)
        {
            var formContent = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("usr", usr) });

            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "citasPasadasById", formContent);

            List<CitasResponse> aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<List<CitasResponse>>(json);
            }
            return aux;
        }


        public static async Task<ResponseCupon> BuscarCuponPOST(string cupon)
        {
            var formContent = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("cupon", cupon)
            });

            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "cupones", formContent);

            ResponseCupon aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<ResponseCupon>(json);
            }

            return aux;
        }

        public static async Task<ResponseAutocompletar> autocompletarGET(string Input)
        {


            HttpClient client = new HttpClient();
            var response = await client.GetAsync(@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + Input + "&language=es&key=AIzaSyB1jbSO9ZuWtLmB1lG6lwDGppOP6oYsoW4&location=19.0412894,-98.20620129999998&radius=15000");

            ResponseAutocompletar aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<ResponseAutocompletar>(json);
            }

            return aux;
        }


        public static async Task<ResponseGeocoding> PositionForAdresses(string Input)
        {


            HttpClient client = new HttpClient();
            var response = await client.GetAsync(@"https://maps.googleapis.com/maps/api/geocode/json?address=" + Input + "&key=AIzaSyB1jbSO9ZuWtLmB1lG6lwDGppOP6oYsoW4");

            ResponseGeocoding aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<ResponseGeocoding>(json);
            }

            return aux;
        }

        public static async Task<ResponseLatLonToAdreess> AdressesForPosition(Position position)
        {


            HttpClient client = new HttpClient();
            string url = @"https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.Latitude.ToString().Replace(',', '.') + "," + position.Longitude.ToString().Replace(',', '.') + "&key=AIzaSyB1jbSO9ZuWtLmB1lG6lwDGppOP6oYsoW4";
            var response = await client.GetAsync(url);

            ResponseLatLonToAdreess aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<ResponseLatLonToAdreess>(json);
            }

            return aux;
        }


    }
}