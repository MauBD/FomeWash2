﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using FomeWash.Models.Utilerias;
using FomeWash.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TK.CustomMap;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class DetalleServicioViewModel : BindableBase, INavigatingAware
    {
        private string direccion;
        Position position;
        public Command Agendar { get; set; }
        private string cupon;
        public Command AppearingCommand { get; set; }
        private List<ResponseServicios> sourceservicios;
        private int servicioPickerIndex=-1;
        private int metodopagoindex=0;
        public INavigationService _navigationService { get; set; }
        private double subtotal;
        private double descuento=0;
        private double total;
        public Command CuponCommand { get; set; }
        public string idCupon { get; set; }

        public double Total
        {
            get { return total; }
            set { total = value;
                RaisePropertyChanged();
            }
        }

        public double Descuento
        {
            get { return descuento; }
            set { descuento = value;
                RaisePropertyChanged();
            }
        }


        public double Subtotal
        {
            get { return subtotal; }
            set { subtotal = value;
                RaisePropertyChanged();
            }
        }


        public DetalleServicioViewModel(INavigationService navigationService)
        {
            Agendar = new Command(async () => await agendarAsync());
            AppearingCommand = new Command(async () => await appearingAsync());
            _navigationService = navigationService;
            CuponCommand = new Command(async () => await checarCuponAsync());
            idCupon = "null";
        }

        public int MetodoPagoIndex
        {
            get { return metodopagoindex; }
            set { metodopagoindex = value;
                RaisePropertyChanged();
            }
        }

        public int ServicioPickerIndex
        {
            get { return servicioPickerIndex; }
            set { servicioPickerIndex = value;
                Subtotal = Convert.ToSingle(SourceServicios[value].Costo.Substring(1));
                Total = Subtotal;
                checarCuponAsync();
                RaisePropertyChanged();
            }
        }
        
        public List<ResponseServicios> SourceServicios
        {
            get { return sourceservicios; }
            set { sourceservicios = value;
                RaisePropertyChanged();
            }
        }


        public string Cupon
        {
            get { return cupon; }
            set { cupon = value;
                RaisePropertyChanged();
            }
        }


        public string Direccion
        {
            get { return direccion; }
            set { direccion = value;
                RaisePropertyChanged();
            }
        }
        private async Task agendarAsync()
        {
            if (MetodoPagoIndex!=-1 && ServicioPickerIndex!=-1)
            {
                ResponseServicios servicio = SourceServicios[ServicioPickerIndex];
                
                UserDialogs.Instance.ShowLoading();
                ResponseAgendar response= await RestClient.agendarCita(Settings.IdUsuarioSettings, DateTime.Now.Date.ToString("yyyy-MM-dd"), Direccion, position.Latitude, position.Longitude, Subtotal.ToString(), idCupon, Descuento, Total.ToString(), MetodoPagoIndex,servicio.IdProductoServicio);
                UserDialogs.Instance.HideLoading();
                if (response.success) {
                    UserDialogs.Instance.Toast("Servicio solicitado, espera la confirmacion");
                    
                }
                else
                {
                    UserDialogs.Instance.Toast("Ocurrio un error");
                }
                (Application.Current.MainPage as SideMenu).IsGestureEnabled = true;
                _navigationService.GoBackToRootAsync();
            }
            else
            {
                UserDialogs.Instance.Alert("¡Debes seleccionar un servicio!");
            }
        }

        async Task appearingAsync() {
            UserDialogs.Instance.ShowLoading();
            
            SourceServicios =await RestClient.getServicios();
            if (SourceServicios!=null)
            {
                ServicioPickerIndex = 0;
            }
            UserDialogs.Instance.HideLoading();
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("direccion"))
            {
                Direccion = (String)parameters["direccion"];
            }

            if (parameters.ContainsKey("position"))
            {
                position = (Position)parameters["position"];
            }
        }

        async Task checarCuponAsync() {
            if (Utilerias.validarString(Cupon))
            {
                UserDialogs.Instance.ShowLoading();
                ResponseCupon cuponusuario = await RestClient.BuscarCuponPOST(Cupon);
                UserDialogs.Instance.HideLoading();
                if (cuponusuario.success)
                {
                    double subtotal = Subtotal;
                    double porcentajedescuento = Convert.ToDouble(cuponusuario.descuento);
                    double descuento = (subtotal * porcentajedescuento) / 100;
                    Descuento = descuento;
                    double total = subtotal - descuento;
                    if (total < 0)
                    {
                        total = 0;
                    }
                    Total = total;
                    idCupon = cuponusuario.IdCupon;
                }
                else
                {
                    UserDialogs.Instance.Alert("El cupon ingresado no es valido :(");
                    Cupon = String.Empty;
                    Total = Subtotal;
                    idCupon = "null";
                }
            }
            else
            {
                Cupon = String.Empty;
                Total = Subtotal;
                Descuento = 0;
                idCupon = "null";
            }
        }
    }
}
