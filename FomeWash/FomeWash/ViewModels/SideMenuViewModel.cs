﻿using Acr.UserDialogs;
using FomeWash.Models.Utilerias;
using FomeWash.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class SideMenuViewModel : ViewModelBase
	{
        public INavigationService navigation;
        public Command NuevaCitaCommand { get; set; }
        public Command AjustesCommand { get; set; }
        public Command MisCitasCommand { get; set; }
        public Command SalirCommand { get; set; }
        public Command ProductosCommand { get; set; }
        public Command PromocionesCommand { get; set; }
        private String nombre;

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value;
                RaisePropertyChanged();
            }
        }

        private String correo;

        public String Correo
        {
            get { return correo; }
            set { correo = value;
                RaisePropertyChanged();
            }
        }

        public Command AppearingCommand { get; set; }

        public SideMenuViewModel(INavigationService navigationService) : base(navigationService)
        {
            navigation = navigationService;
            AppearingCommand = new Command(datosUsuario);
            NuevaCitaCommand = new Command(async () => await nuevaCita());
            AjustesCommand = new Command(async () => await ajustes());
            MisCitasCommand = new Command(async () => await misCitas());
            SalirCommand = new Command(salir);
            ProductosCommand = new Command(async () => await ProductosAsync());
            PromocionesCommand = new Command(async () => await Promociones());
        }

        void datosUsuario() {
            Nombre = Settings.NombreSettings;
            Correo = Settings.CorreoSettings;
        }

        public async Task nuevaCita()
        {
            if (Plugin.Geolocator.CrossGeolocator.IsSupported)
            {
                if (Plugin.Geolocator.CrossGeolocator.Current.IsGeolocationAvailable &&
                Plugin.Geolocator.CrossGeolocator.Current.IsGeolocationEnabled)
                {
                    await navigation.NavigateAsync("NavigationPage/Welcome");
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    UserDialogs.Instance.Alert("Por favor activa el gps e intenta de nuevo");
                }

            }
            else
            {
                UserDialogs.Instance.Alert("Parece que tu dispositivo no cuenta con gps");
            }

        }

        public async Task ajustes()
        {
            UserDialogs.Instance.ShowLoading();
            await navigation.NavigateAsync("NavigationPage/Ajustes");
            UserDialogs.Instance.HideLoading();
        }

        public async Task misCitas()
        {
            await navigation.NavigateAsync("NavigationPage/Historial");

        }

        public void salir()
        {
            UserDialogs.Instance.ShowLoading();
            Settings.CorreoSettings = string.Empty;
            Settings.IdUsuarioSettings = string.Empty;
            Settings.NombreSettings = string.Empty;
            Settings.PasswordSettings = string.Empty;
            Application.Current.MainPage = new Login();
            UserDialogs.Instance.HideLoading();
        }

        public async Task ProductosAsync()
        {
            UserDialogs.Instance.ShowLoading();
            await navigation.NavigateAsync("NavigationPage/Productos");
            UserDialogs.Instance.HideLoading();
        }

        public async Task Promociones()
        {
            UserDialogs.Instance.ShowLoading();
            await navigation.NavigateAsync("NavigationPage/Promociones");
            UserDialogs.Instance.HideLoading();
        }
    }
}
