﻿using FomeWash.Models.Utilerias;
using FomeWash.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
    

    public class WelcomeViewModel : ViewModelBase
	{
        private String _MsgBienv;

        public String MsgBienv
        {
            get { return _MsgBienv; }
            set
            {
                _MsgBienv = value;
                RaisePropertyChanged();
            }
        }
        private INavigationService _localNavigation;
        public Command NuevoServicioCommand { get; set; }
        public Command AppearingCommand { get; set; }

        void NuevoServicio() {
            
            _localNavigation.NavigateAsync("Mapa");
        }

        public WelcomeViewModel(INavigationService navigationService) : base(navigationService)
        {
            _localNavigation = navigationService;
            NuevoServicioCommand = new Command(NuevoServicio);
            AppearingCommand = new Command(appearing);
            int a = Settings.NombreSettings.IndexOf(' ');
            MsgBienv = "Hola "+Settings.NombreSettings.Substring(0,a);
        }

        void appearing() {
            (Application.Current.MainPage as SideMenu).IsGestureEnabled = true;
        }
    }
}
