﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using FomeWash.Models.Utilerias;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class ServiciosPendientesViewModel : BindableBase
	{
        public ServiciosPendientesViewModel()
        {
            AppearingCommand = new Command(async () => await AppearingAsync());
        }

        private List<CitasResponse> source;
        public Command AppearingCommand { get; set; }

        public List<CitasResponse> ServiciosSource
        {
            get { return source; }
            set
            {
                source = value;
                RaisePropertyChanged();
            }
        }
        

        private async Task AppearingAsync()
        {
            UserDialogs.Instance.ShowLoading();
            ServiciosSource = await RestClient.getCitasById(Settings.IdUsuarioSettings);
            UserDialogs.Instance.HideLoading();
        }
    }
}
