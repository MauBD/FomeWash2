﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using FomeWash.Models.Utilerias;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class AjustesViewModel : BindableBase
	{
        private string nombre;
        private string correo;
        private string image;
        private string contraseña;
        private string nuevacontraseña;
        public Command SalirCommand { get; set; }
        public Command CambiarCommand { get; set; }
        INavigationService _navigationService { get; set; }

        public AjustesViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            Image = Settings.FotoSettings;
            Nombre = Settings.NombreSettings;
            Correo = Settings.CorreoSettings;
            CambiarCommand = new Command(async () => await cambiarpwd());

        }
        public string NuevaContraseña
        {
            get { return nuevacontraseña; }
            set
            {
                nuevacontraseña = value;
                RaisePropertyChanged();
            }
        }

        public string Contraseña
        {
            get { return contraseña; }
            set
            {
                contraseña = value;
                RaisePropertyChanged();
            }
        }

        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                RaisePropertyChanged();
            }
        }

        public string Correo
        {
            get { return correo; }
            set
            {
                correo = value;
                RaisePropertyChanged();
            }
        }

        public string Nombre
        {
            get { return nombre; }
            set
            {
                nombre = value;
                RaisePropertyChanged();
            }
        }

        
        async Task cambiarpwd()
        {
            if (Utilerias.validarString(Contraseña) && Utilerias.validarString(NuevaContraseña))
            {
                if (Contraseña != NuevaContraseña)
                {

                    RegistroModel response = await RestClient.cambiarPwd(Convert.ToInt32(Settings.IdUsuarioSettings), Contraseña, NuevaContraseña);
                    UserDialogs.Instance.Toast(response.msg);
                    if (response.success)
                    {
                        await _navigationService.NavigateAsync("../../Login");
                    }
                    
                }
                else
                {
                    UserDialogs.Instance.Toast("¡La nueva contraseña debe ser diferente a la contrasela actual!");
                }
            }
            else
            {
                UserDialogs.Instance.Toast("¡Verifica tus datos!");
            }
        }


    }
}
