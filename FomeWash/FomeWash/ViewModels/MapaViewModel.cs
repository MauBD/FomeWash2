﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using FomeWash.Models.Utilerias;
using FomeWash.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TK.CustomMap;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
    public class MapaViewModel : BindableBase, IDestructible
    {
        private MapSpan mapregion;
        public Plugin.Geolocator.Abstractions.IGeolocator locator { get; set; }
        public Command Appearing { get; set; }
        private TKCustomMapPin pin;
        private List<TKCustomMapPin> _Pins;
        public Command CentrarClicked { get; set; }
        private string direccion;
        public Command TextChanged { get; set; }
        private string input;
        private List<Prediction> predicciones;
        private bool listaisvisible;
        public Command MapClickedCommand { get; set; }
        private DelegateCommand<Prediction> _selItemCommand;
        public DelegateCommand<Prediction> SelectItemCommand => _selItemCommand ?? (_selItemCommand = new DelegateCommand<Prediction>(selectedItem));
        Prediction prediction;
        public Command ContinuarCommand { get; set; }
        public INavigationService _navigationService { get; set; }

        private async void selectedItem(Prediction paramData)
        {
            prediction = paramData;
            Input = paramData.description;
            ListaIsVisible = false;
            UserDialogs.Instance.ShowLoading();
            ResponseGeocoding aux = await RestClient.PositionForAdresses(Input);
            MapRegion = MapSpan.FromCenterAndRadius(new Position(aux.results[0].geometry.location.lat, aux.results[0].geometry.location.lng), Distance.FromMeters(50));
            Pin.Position = new Position(aux.results[0].geometry.location.lat, aux.results[0].geometry.location.lng);
            UserDialogs.Instance.HideLoading();
        }

        public bool ListaIsVisible
        {
            get { return listaisvisible; }
            set
            {
                listaisvisible = value;
                RaisePropertyChanged();
            }
        }


        public List<Prediction> Predicciones
        {
            get { return predicciones; }
            set
            {
                predicciones = value;
                RaisePropertyChanged();
            }
        }


        public string Input
        {
            get { return input; }
            set
            {
                input = value;
                RaisePropertyChanged();
            }
        }

        public string Direccion
        {
            get { return direccion; }
            set
            {
                direccion = value;
                RaisePropertyChanged();
            }
        }

        public MapaViewModel(INavigationService navigationService)
        {
            locator = Plugin.Geolocator.CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            MapRegion = MapSpan.FromCenterAndRadius(new Position(19.0412894, -98.20620129999998), Distance.FromMeters(50));
            Appearing = new Command(async () => await appearingAsync());
            CentrarClicked = new Command(async () => await centrargpsAsync());
            TextChanged = new Command(async () => await autocompletarAsync());
            MapClickedCommand = new Command(mapclick);
            ContinuarCommand = new Command(async () => await irAResumenAsync());
            _navigationService = navigationService;


        }

        private async Task irAResumenAsync()
        {
            if (!String.IsNullOrEmpty(Input) && !String.IsNullOrWhiteSpace(Input)) {

                var parameters = new NavigationParameters
                {
                    { "direccion", Input },
                    { "position", Pin.Position}
                };
                await _navigationService.NavigateAsync("DetalleServicio",parameters);
            }
            
        }

        private void mapclick(object obj)
        {
            ListaIsVisible = false;
        }

        public List<TKCustomMapPin> Pins
        {
            get { return _Pins; }
            set
            {
                _Pins = value;
                RaisePropertyChanged();
            }
        }

        public TKCustomMapPin Pin
        {
            get { return pin; }
            set
            {
                pin = value;
                RaisePropertyChanged();
            }
        }

        public MapSpan MapRegion
        {
            get { return mapregion; }
            set
            {
                mapregion = value;                
                RaisePropertyChanged();
            }
        }

        async Task appearingAsync()
        {
            UserDialogs.Instance.ShowLoading();
            (Application.Current.MainPage as SideMenu).IsGestureEnabled = false;
            Plugin.Geolocator.Abstractions.Position pos = await locator.GetPositionAsync();
            Pins = new List<TKCustomMapPin>();
            Pin = new TKCustomMapPin();
            Pin.IsDraggable = true;
            Pin.Position = new Position(pos.Latitude, pos.Longitude);
            Pins.Add(Pin);
            ResponseLatLonToAdreess aux = await RestClient.AdressesForPosition(Pin.Position);
            Input = aux.results[0].formatted_address;
            MapRegion = MapSpan.FromCenterAndRadius(new Position(pos.Latitude, pos.Longitude), Distance.FromMeters(50));
            UserDialogs.Instance.HideLoading();
        }

        async Task centrargpsAsync()
        {
            UserDialogs.Instance.ShowLoading();
            Plugin.Geolocator.Abstractions.Position pos = await locator.GetPositionAsync();
            Pin.Position = new Position(pos.Latitude, pos.Longitude);
            ResponseLatLonToAdreess aux = await RestClient.AdressesForPosition(Pin.Position);
            Input = aux.results[0].formatted_address;
            //int res = await obtenerDireccionAsync(new Position(pos.Latitude, pos.Longitude));
            //MapRegion = MapSpan.FromCenterAndRadius(new Position(pos.Latitude, pos.Longitude), Distance.FromMeters(50));
            UserDialogs.Instance.HideLoading();
            //alert(res);
        }

        public Command<TKCustomMapPin> PinDragEndCommand
        {
            get
            {
                return new Command<TKCustomMapPin>(async pin =>
                {
                    UserDialogs.Instance.ShowLoading();
                    Position p = pin.Position;
                    //int res = await obtenerDireccionAsync(new Position(p.Latitude, p.Longitude));
                    ResponseLatLonToAdreess aux = await RestClient.AdressesForPosition(p);

                    //prediction = new Prediction();
                    //prediction.description = aux.results[0].formatted_address;
                    Input = aux.results[0].formatted_address;
                    UserDialogs.Instance.HideLoading();
                    //MapRegion = MapSpan.FromCenterAndRadius(new Position(p.Latitude, p.Longitude), Distance.FromMeters(50));

                });
            }
        }

        async Task<int> obtenerDireccionAsync(Position p)
        {

            IEnumerable<Plugin.Geolocator.Abstractions.Address> addressList;
            if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
            {
                UserDialogs.Instance.ShowLoading();
                addressList = await locator.GetAddressesForPositionAsync(new Plugin.Geolocator.Abstractions.Position(p.Latitude, p.Longitude));

                UserDialogs.Instance.HideLoading();
                if (addressList.Count() > 0)
                {
                    Plugin.Geolocator.Abstractions.Address aux = addressList.ElementAt(0);
                    Direccion = aux.Thoroughfare + ", " + aux.SubThoroughfare + ", " + aux.Locality + ", " + aux.SubLocality;
                    return 1;
                }
                else
                {
                    Direccion = null;
                    return 0;
                }
            }
            else
            {
                Direccion = null;
                return -1;
            }
        }

        void alert(int i)
        {
            if (i == 0)
            {
                UserDialogs.Instance.Alert("No pudimos encontrar direcciones en esta ubicacion");
            }
            else if (i == -1)
            {
                UserDialogs.Instance.Alert("Por favor activa la ubicacion");
            }
        }

        async Task autocompletarAsync()
        {
            if (Input.Length > 5)
            {
                if (prediction != null)
                {
                    if (Input != prediction.description)
                    {
                        ResponseAutocompletar aux = await RestClient.autocompletarGET(Input);
                        if (aux != null)
                        {
                            Predicciones = aux.predictions;
                            ListaIsVisible = true;
                        }
                    }
                }
                else
                {
                    ResponseAutocompletar aux = await RestClient.autocompletarGET(Input);
                    if (aux != null)
                    {
                        Predicciones = aux.predictions;
                        ListaIsVisible = true;
                    }
                }

            }
        }

        public void Destroy()
        {

        }
    }
}
