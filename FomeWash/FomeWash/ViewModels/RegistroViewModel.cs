﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using FomeWash.Models.Utilerias;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
    public class RegistroViewModel : ViewModelBase
    {
        public Command RegistroCommand { get; set; }
        public INavigationService Navigation { get; set; }
        private IList<Escuela> escuela;
        private string nombre;
        private string correo;
        private string pwd;
        private string pwd2;
        private bool alertnombre;
        private bool alertmail;
        private bool alertTel;
        private bool alertpwd;
        private bool alertpwd2;
        private bool alertmsg;
        private string telefono;

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value;
                AlertTel = false;
                RaisePropertyChanged();
            }
        }


        // constructor :v 
        public RegistroViewModel(INavigationService navigationService) : base(navigationService)
        {
            Navigation = navigationService;
            //definimos que metodos van a ultilizar los Command
            RegistroCommand = new Command(async () => await registro());
        }



        //propiedades encargadas de mostrar los mensajes de alerta
        public bool alertMsg
        {
            get { return alertmsg; }
            set
            {
                alertmsg = value;
                RaisePropertyChanged();
            }
        }

        public bool alertNombre
        {
            get { return alertnombre; }
            set
            {
                alertnombre = value;
                RaisePropertyChanged();
            }
        }

        public bool alertMail
        {
            get { return alertmail; }
            set
            {
                alertmail = value;
                alertMsg = false;
                RaisePropertyChanged();
            }
        }

        public bool AlertTel
        {
            get { return alertTel; }
            set
            {
                alertTel = value;
                RaisePropertyChanged();
            }
        }

        public bool alertPwd
        {
            get { return alertpwd; }
            set
            {
                alertpwd = value;
                RaisePropertyChanged();
            }
        }


        public bool alertPwd2
        {
            get { return alertpwd2; }
            set
            {
                alertpwd2 = value;
                RaisePropertyChanged();
            }
        }


        

        public string PWD2
        {
            get { return pwd2; }
            set
            {
                pwd2 = value;
                alertPwd2 = false;
                RaisePropertyChanged();
            }
        }

        public string PWD
        {
            get { return pwd; }
            set
            {
                pwd = value;
                alertPwd = false;
                RaisePropertyChanged();
            }
        }

        public IList<Escuela> Escuelas
        {
            get { return escuela; }
            set
            {
                escuela = value;
                RaisePropertyChanged();
            }
        }

        public string Correo
        {
            get { return correo; }
            set
            {
                correo = value;
                alertMail = false;
                RaisePropertyChanged();
            }
        }

        public string Nombre
        {
            get { return nombre; }
            set
            {
                nombre = value;
                alertNombre = false;
                RaisePropertyChanged();
            }
        }

        public async Task registro()
        {

            UserDialogs.Instance.ShowLoading("Registrando...");
            if (!String.IsNullOrWhiteSpace(Nombre) && Utilerias.validarcorreo(Correo) && !String.IsNullOrEmpty(Telefono)
                && !String.IsNullOrWhiteSpace(PWD) && !String.IsNullOrWhiteSpace(PWD2))
            {

                if (PWD != PWD2)
                {
                    alertPwd2 = true;
                }
                else
                {
                    RegistroModel a = await RestClient.newRegistro(Nombre, Correo, Telefono, PWD);
                    if (!(a is null))
                    {
                        if (a.success)
                        {
                            UserDialogs.Instance.Toast("Registro exitoso!");
                            //await Navigation.PopAsync();
                            await Navigation.GoBackAsync();
                        }
                        else
                        {
                            alertMsg = true;
                        }
                    }
                }
            }
            else
            {
                if (String.IsNullOrEmpty(Nombre))
                {
                    alertNombre = true;
                }
                if (String.IsNullOrEmpty(Telefono))
                {
                    AlertTel= true;
                }

                if (!Utilerias.validarcorreo(Correo))
                {
                    alertMail = true;
                }
                if (string.IsNullOrWhiteSpace(PWD))
                {
                    alertPwd = true;
                }
                if (string.IsNullOrWhiteSpace(PWD2))
                {
                    alertPwd2 = true;
                }
            }
            //ActIndicator = false;
            UserDialogs.Instance.HideLoading();
        }

        

    }
}
