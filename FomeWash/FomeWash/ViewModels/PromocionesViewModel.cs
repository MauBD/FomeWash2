﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class PromocionesViewModel : BindableBase
	{
        public Command AppearingCommand { get; set; }
        private List<ResponsePromos> source;

        public List<ResponsePromos> PromosSource
        {
            get { return source; }
            set { source = value;
                RaisePropertyChanged();
            }
        }

        private bool _ImgIsVisible;

        public bool ImgIsVisible
        {
            get { return _ImgIsVisible; }
            set { _ImgIsVisible = value;
                RaisePropertyChanged();
            }
        }

        private bool _LvIsVisible;

        public bool LvIsVisible
        {
            get { return _LvIsVisible; }
            set { _LvIsVisible = value;
                RaisePropertyChanged();
            }
        }



        public PromocionesViewModel()
        {
            AppearingCommand = new Command(async ()=> await Appearing());
        }

        private async Task Appearing()
        {
            UserDialogs.Instance.ShowLoading();
            PromosSource = await RestClient.getPromociones();
            if (PromosSource.Count==0)
            {
                LvIsVisible = false;
                ImgIsVisible = true;
            }
            else {
                LvIsVisible = true;
                ImgIsVisible = false;
            }
            UserDialogs.Instance.HideLoading();
        }
    }
}
