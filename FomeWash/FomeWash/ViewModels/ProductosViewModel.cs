﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class ProductosViewModel : BindableBase
	{
        public Command AppearingCommand { get; set; }

        private List<ResponseServicios> source;

        public List<ResponseServicios> ProductosSource
        {
            get { return source; }
            set { source = value;
                RaisePropertyChanged();
            }
        }


        public ProductosViewModel()
        {
            AppearingCommand = new Command(async () => await appearingAsync());
        }

        async Task appearingAsync() {
            UserDialogs.Instance.ShowLoading();
            ProductosSource = await RestClient.getServicios();
            UserDialogs.Instance.HideLoading();
        }
	}
}
