﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using FomeWash.Models.Utilerias;
using FomeWash.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class InicioViewModel : ViewModelBase
    {
        public Command Appearing { get; set; }
        private double imagescale=1;
        private INavigationService _navigationService;

        public double ImageScale
        {
            get { return imagescale; }
            set
            {
                imagescale = value;
                RaisePropertyChanged();
            }
        }

        public InicioViewModel(INavigationService navigationService): base(navigationService)
        {
            _navigationService = navigationService;
            Appearing = new Command(async ()=>await appearingAsync());
        }

        async Task appearingAsync()
        {
            await EscalarAsync();
            await Login();
        }

        async Task EscalarAsync()
        {
            for (double i = 0; i <= 1.5; i+=.1)
            {
                await Task.Delay(50);
                ImageScale = i;
            }
        }
        async Task Login()
        {
            
            if (Settings.CorreoSettings!=String.Empty)
            {
                ResponseLogin a = await RestClient.login(Settings.CorreoSettings, Settings.PasswordSettings);
                if (a.exito)
                {
                    /*SideMenu menu = new SideMenu();
                    menu.Detail = new Welcome();
                    Application.Current.MainPage=new NavigationPage(menu);*/
                    await _navigationService.NavigateAsync("/SideMenu/NavigationPage/Welcome");
                }
                else
                {

                    Application.Current.MainPage = new Login();
                    //await _navigationService.NavigateAsync("../Login");
                }
            }
            else
            {
                Application.Current.MainPage = new Login();
                //await _navigationService.NavigateAsync("../Login");
            }
        }
    }
}
