﻿using Acr.UserDialogs;
using FomeWash.Models;
using FomeWash.Models.Responses;
using FomeWash.Models.Utilerias;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWash.ViewModels
{
	public class MisServiciosViewModel : BindableBase
	{
        private List<CitasResponse> source;

        public List<CitasResponse> ServiciosSource
        {
            get { return source; }
            set { source = value;
                RaisePropertyChanged();
            }
        }
        public Command AppearingCommand { get; set; }

        public MisServiciosViewModel()
        {
            AppearingCommand = new Command(async ()=> await AppearingAsync());
        }

        private async Task AppearingAsync()
        {
            UserDialogs.Instance.ShowLoading();
            ServiciosSource = await RestClient.getCitasById(Settings.IdUsuarioSettings);
            UserDialogs.Instance.HideLoading();
        }
    }
}
